import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
	mode: "history",
	routes: [
		{
			name: "main",
			path: "/",
			component: () => import("@/pages/SplitDaBill.vue")
		},
		{
			name: "create-party",
			path: "/party/:partyId",
			component: () => import("@/pages/CreateParty.vue")
		},
		{
			name: "party-detail",
			path: "/party/:partyId/detail",
			component: () => import("@/pages/PartyDetail.vue")
		}
	]
});