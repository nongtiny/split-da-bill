import {
	UPDATE_STEPS
} from "../mutationTypes";

const state = {
	steps: ["member"]
};

const mutations = {
	[UPDATE_STEPS](stateData, data) {
		stateData.steps = [
			...data
		];
	}
};

const actions = {
	addStep({ commit, state: stateData }, data) {
		if (stateData.steps.includes(data)) {
			return;
		}

		const payload = [
			...stateData.steps,
			data
		];

		commit(UPDATE_STEPS, payload);
	},
	deleteStep({ commit, state: stateData }, data) {
		const payload = stateData.steps.filter((step) => step !== data);
		commit(UPDATE_STEPS, payload);
	},
	clearStep({ commit }) {
		commit(UPDATE_STEPS, ["member"]);
	}
};

export default {
	namespaced: true,
	state,
	mutations,
	actions
};