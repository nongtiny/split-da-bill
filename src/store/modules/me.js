import {
	UPDATE_TELNUM,
	UPDATE_PARTIES
} from "../mutationTypes";

const state = {
	telNumber: "",
	parties: []
};

const mutations = {
	[UPDATE_TELNUM](stateData, data) {
		stateData.telNumber = data;
	},
	[UPDATE_PARTIES](stateData, data) {
		stateData.parties = [
			...data
		];
	}
};

const actions = {
	updateTelNumber({ commit }, data) {
		commit(UPDATE_TELNUM, data);
	},
	updateParties({ commit, state: stateData }, data) {
		const parties = stateData.parties;

		if (data.edit) {
			const editParty = parties.map((p) => {
				if (p.name === data.name) {
					return {
						...data,
						edit: false
					};
				}

				return {
					...p,
					edit: false
				};
			});

			commit(UPDATE_PARTIES, editParty);
			return;
		}

		const payload = [
			...parties,
			data
		];

		commit(UPDATE_PARTIES, payload);
	},
	deleteParty({ commit, state: stateData }, data) {
		const payload = stateData.parties.filter((party) => party.name !== data);
		commit(UPDATE_PARTIES, payload);
	}
};

const getters = {
	/* eslint-disable no-shadow */
	getPartyByName: (state) => (name) => {
		return state.parties.find((p) => p.name === name);
	}
};

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
};