import {
	UPDATE_PARTY,
	UPDATE_ORDERS
} from "../mutationTypes";

const state = {
	party: {
		name: "",
		totalCost: 0,
		splitBills: []
	},
	orders: []
};

const mutations = {
	[UPDATE_PARTY](stateData, data) {
		stateData.party = {
			...data
		};
	},
	[UPDATE_ORDERS](stateData, data) {
		stateData.orders = [
			...data
		];
	}
};

const actions = {
	createMembers({ commit, state: stateData }, data) {
		const { members } = data;
		const partyData = stateData.party;
		const partyDataBills = stateData.party.splitBills;
		const newMembers = [];

		members.forEach((m, index) => {
			const isEdit = partyDataBills.length > 0 && partyDataBills[index] && partyDataBills[index].haveToPaid;
			if (isEdit) {
				newMembers.push({
					name: m.name,
					menus: partyDataBills[index].menus,
					haveToPaid: partyDataBills[index].haveToPaid,
					paid: partyDataBills[index].paid,
					change: partyDataBills[index].change,
					isPaid: partyDataBills[index].isPaid
				});
			} else {
				newMembers.push({
					name: m.name,
					menus: [],
					haveToPaid: 0,
					paid: 0,
					change: 0,
					isPaid: false
				});
			}
		});

		const partyMemberNames = newMembers.map((m) => m.name);
		const result = newMembers.filter((mem, index) => partyMemberNames.indexOf(mem.name) === index);

		const payload = {
			...partyData,
			splitBills: result
		};

		commit(UPDATE_PARTY, payload);
	},
	updateOrders({ commit }, data) {
		commit(UPDATE_ORDERS, data);
	},
	createParty({ commit, rootState }, data) {
		const parties = rootState.me.parties;
		const isDuplicateName = parties.some((party) => {
			if (!party.name) {
				return false;
			}

			return party.name.toLowerCase() === data.toLowerCase();
		});

		if (!isDuplicateName) {
			const payload = {
				name: data,
				totalCost: 0,
				splitBills: [],
				edit: false
			};

			commit(UPDATE_PARTY, payload);
			return true;
		}

		commit(UPDATE_ORDERS, []);
		return false;
	},
	editParty({ commit }, data) {
		const payload = {
			name: data.name,
			totalCost: data.totalCost,
			splitBills: data.splitBills,
			edit: true
		};

		commit(UPDATE_PARTY, payload);
		commit(UPDATE_ORDERS, data.orders);
	}
};

export default {
	namespaced: true,
	state,
	mutations,
	actions
};