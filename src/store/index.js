import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";

import party from "./modules/party";
import me from "./modules/me";
import step from "./modules/step";

Vue.use(Vuex);

const partyVuexLocal = new VuexPersist({
	key: "me",
	reducer: (state) => ({
		me: state.me
	})
});

export default new Vuex.Store({
	modules: {
		party,
		me,
		step
	},
	plugins: [partyVuexLocal.plugin]
});