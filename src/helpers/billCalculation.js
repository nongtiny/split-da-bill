/* eslint-disable import/prefer-default-export */

export function calculateMenuSplitCost(orders, persons) {
	const personCosts = persons;
	// Reset
	personCosts.forEach((person) => {
		person.haveToPaid = 0;
	});
	// Compute the cost for each person
	orders.forEach((menu) => {
		personCosts.forEach((person) => {
			if (menu.members.includes(person.name)) {
				person.haveToPaid += Math.round(menu.cost) / menu.members.length;
				// const total = person.haveToPaid;
				// person.change = parseInt(person.paid, 10) - total;

				// if (person.haveToPaid > parseInt(person.paid, 10)) {
				// 	person.isPaid = true;
				// }
			}
		});
	});

	return { personCosts };
}